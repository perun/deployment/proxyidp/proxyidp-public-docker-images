# ProxyIdP public docker images

Public images used in ProxyIdP.

See the container registry for available images and tags.

## docker-gdi

Simple docker image with [LS AAI mock](https://gitlab.ics.muni.cz/perun/perun-proxyidp/bleeding-edge/lsaai-oidc-mock),
based on Tomcat 9 running on 8009 port.

The container assumes that it is behind a reverse proxy which only handles HTTPS requests.

### Volumes

It is suggested to mount these volumes:

- `/etc/lsaai-mock/` - directory with all configuration

### Databases

This docker expects a MySQL Database to be available for both applications running in it.
